export interface Note {
  id: number;
  title: string;
  isComplete: boolean;
}
