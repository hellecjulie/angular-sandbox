import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotesService } from '../../../../services/notes.service';

import { NoteListModule } from '../note-list/note-list.module';
import { NoteInputModule } from '../note-input/note-input.module';
import { NotesComponent } from './notes.component';

@NgModule({
  imports: [CommonModule, NoteListModule, NoteInputModule],
  declarations: [NotesComponent],
  exports: [NotesComponent],
  providers: [NotesService]
})
export class NotesModule {}
