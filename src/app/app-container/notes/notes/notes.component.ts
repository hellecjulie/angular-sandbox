import { Component, OnInit } from '@angular/core';
import { NotesService } from '../../../../services/notes.service';
import { Note } from '../../../../models/notes.interface';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
})
export class NotesComponent implements OnInit {
  private notes: Note[];

  constructor(private data: NotesService) {}

  ngOnInit() {
    this.data.currentNotes.subscribe((notes) => (this.notes = notes));
  }
}
