import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesService } from '../../../../services/notes.service';
import { NoteInputModule } from '../note-input/note-input.module';
import { NoteListModule } from '../note-list/note-list.module';

import { NotesComponent } from './notes.component';

describe('NotesComponent', () => {
  let component: NotesComponent;
  let fixture: ComponentFixture<NotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NoteInputModule, NoteListModule],
      declarations: [NotesComponent],
      providers: [NotesService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
