import { Component, OnInit } from '@angular/core';
import { NotesService } from '../../../../services/notes.service';
import { Note } from '../../../../models/notes.interface';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss'],
})
export class NoteListComponent implements OnInit {
  private notes: Note[];
  constructor(private data: NotesService) {}

  deleteNote = (id: number) => {
    this.data.deleteNote(id);
  };

  toggleNote = (id: number) => {
    this.data.toggleCompleteNoteState(id);
  };

  ngOnInit() {
    this.data.currentNotes.subscribe((notes) => (this.notes = notes));
  }
}
